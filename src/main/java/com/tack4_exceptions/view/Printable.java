package com.tack4_exceptions.view;

/**
 * Printable Interface
 */
@FunctionalInterface
public interface Printable {

    void print();
}
