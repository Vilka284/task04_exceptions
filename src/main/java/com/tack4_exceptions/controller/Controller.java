package com.tack4_exceptions.controller;

import com.tack4_exceptions.model.Plane;

import java.util.ArrayList;

/**
 * Controller Interface
 */
public interface Controller {

    /**
     * @param planeArrayList set list of planes
     */
    void setPlaneArrayList(ArrayList<Plane> planeArrayList);

    /**
     * @return list of planes
     */
    ArrayList<Plane>  getPlaneArrayList();
}
