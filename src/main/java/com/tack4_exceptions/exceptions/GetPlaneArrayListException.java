package com.tack4_exceptions.exceptions;

public class GetPlaneArrayListException extends RuntimeException {

    public GetPlaneArrayListException() throws Exception{
        super("Array List might be empty");
    }
}
