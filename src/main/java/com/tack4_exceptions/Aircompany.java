package com.tack4_exceptions;


import com.tack4_exceptions.view.View;

/**
 * Air company - build and manage your planes
 */
public class Aircompany {

    /**
     * <b>Actions you can do:</b><br>
     * 1. Check planes<br>
     * 2. Add plane<br>
     * 3. Sort by flight distance<br>
     * 4. Find by fuel tank capacity<br>
     *
     * @param args - args
     * @author Andrii Syd
     */
    public static void main(String[] args) {
        new View().show();
    }

}
